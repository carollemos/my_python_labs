# Make a program that reads a number from 0 to 9999 and shows each of the separate digits on the screen.
#Ex: Enter a number: 1834
# Unit: 4
# Ten: 3
# Hundred: 8
# Thousand: 1

#Faça um programa que leia um número de 0 a 9999 e mostre na tela cada um dos dígitos separados.
#Ex: Digite um número: 1834
# Unidade:4
# Dezena: 3
# Centena: 8
# Milhar: 1

number = int(input('Write a number: '))
unity = number % 10
ten = number // 10 % 10
hundred = number // 100 % 10
thousands = number // 1000 % 10

print(f'Unity:{unity}')
print(f'Ten: {ten}')
print(f'Hundred: {hundred}')
print(f'Thousands: {thousands}')






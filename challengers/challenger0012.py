# Make an algorithm that reads the price of a product and shows its new price, with a 5% discount.

#Faça um algorítimo que leia o preço de um produto e mostre seu novo preço, com 5% de desconto.

prod = float(input('Enter the product value: '))
porc = 5 / 100 * prod
discount = prod - porc


print("The discounted value of 5% is: R${:.2f}".format(discount))
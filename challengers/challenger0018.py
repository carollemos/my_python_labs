# Make a program that reads any angle and shows on the screen the value of sine, cosine and
# tangent of this angle.

# Faça um programa que leia um ângulo qualquer e mostre na tela o valor do seno, cosseno e
# tangente desse ângulo.

from math import cos, tan, sin, radians
angle = float(input('Enter the desired angle: '))

sine = sin(radians(angle))
print(f'The SINE of the angle {angle}º is: {sine:.2f}')

cosine = cos(radians(angle))
print(f'The COSINE of the angle {angle}º is: {cosine:.2f}')

tangent = tan(radians(angle))
print(f'The TANGENT of the angle {angle}º is: {tangent:.2f}')
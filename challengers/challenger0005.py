#Make a program that reads a whole number and shows your successor and predecessor on the screen.

#Faça um programa que leia um número inteiro e mostre na tela o seu sucessor e seu antecessor.

n1 = int(input('Enter a number: '))
successor = n1 + 1
predecessor = n1 - 1

print('Your number is: {} \n The successor is: {} \n The predecessor is: {}'.format(n1, successor, predecessor))


print('-=-'*6)
print('Your code worked!')
print('-=-'*6)
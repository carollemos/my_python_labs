#Read two numbers and add them together.

#Leia dois número e faça a soma entre eles.

n1 = float(input('Enter the first number:'))
n2 = float(input('Enter the second number:'))
sum = n1 + n2
print('The sum between the numbers is: {:.2f}'.format(sum))

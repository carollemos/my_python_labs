# Make a program that reads a sentence on the keyboard and shows:
#> How many times does the letter "A" appear.
#> In what position does it appear the first time.
#> IN what position it appears last.

# Faça um programa que leia uma frase pelo teclado e mostre:
#> Quantas vezes aparece a letra "A".
#> Em que posição ela aparece a primeira vez.
#> EM que posição ela aparece a última vez.

phrase = input('Enter a phrase: ').strip().lower()
A = phrase.count('a')
print(f'The letter A appears {A} times in the sentence.')

first_position = phrase.find('a')
print(f'The letter A was found the first time in the {phrase}.')

last_position = phrase.rfind('a')
print(f'The letter A was found the last time in the position {last_position}.')

#Develop a program that asks for the distance of a trip in Km.
#Calculate the ticket price, charging R $ 0.50 per km for trips of up to 200 km
# and R $ 0.45 for longer trips.

#Desenvolva um programa que pergunte a distância de uma viagem em Km.
#Calcule o preço da passagem, cobrando R$0,50 por km para viagens de até 200 km
# e R$0,45 por viagens mais longas.

trip = float(input('How many KMs have you traveled? '))
if trip <= 200:
    print(f'Your trip cost R${trip * 0.50:.1f} reais')
else:
    print(f'Your trip cost R${trip * 0.45:.1f} reais')

#Develop a program that reads a student's two grades, calculates and displays their average.

#Desenvolva um programa que leia as duas notas de um aluno, calcule e mostre a sua média.

note01 = float(input('Write your first note: '))
note02 = float(input('Write your second note: '))
average = (note01 + note02) / 2

print('Your average is {:.1f}'.format(average))

if average >= 7:
    print("You was aproved!")
else:
    print("You was reproved!")

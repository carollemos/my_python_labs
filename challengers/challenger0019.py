# A teacher wants to raffle one of his four students to finish the picture. Make a program
# help him by reading his name and writing the name of the chosen one.

# Um professor quer sortear um dos seus quatro alunos para agagar o quadro. Faça um programa
# que ajude ele, lendo o nome dele e escrevendo o nome do escolhido.

from random import choice

student01 = input('Enter a students name: ')
student02 = input('Enter another students name: ')
student03 = input('Enter another students name: ')
student04 = input('Enter another students name: ')
students = [student01, student02, student03, student04]
selected = choice(students)

print(f'The chosen student was: {selected}.')



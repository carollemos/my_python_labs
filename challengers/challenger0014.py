#Write a program that converts a temperature by typing in degrees Celsius and converting it to degrees Fahrenheit.

#Escreva um programa que converta uma temperatura digitando em graus Celsius e converta para graus Fahrenheit.

c = float(input('Write the temperature on Celsius: '))
f = 1.8 * c + 32

print('Your result is {}º on Fahrenheit.'.format(f))
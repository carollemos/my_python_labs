#Write a program that asks an employee's salary and calculates the amount
#of your raise.
#For salaries in excess of R $ 1,250.00, calculate an increase of 10%.
#For lower or equal wages, the increase is 15%.

#Escreva um programa que pergunte o salário de um funcionário e calcule o valor
#do seu aumento.
#Para salários superiores a R$1.250,00, calcule um aumento de 10%.
#Para salários menores ou iguais, o aumento é de 15%.

salary = float(input('What is your salary? '))
increase1 = salary * 10 / 100
if salary > 1250:
    print(f'Your new salary is: {increase1 + salary:.3f},00')
else:
    increase2 = salario * 15 / 100
    print(f'Your new salary is: {increase2 + salary:.3f},00')
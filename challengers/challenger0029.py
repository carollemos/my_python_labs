#Write a program that reads the speed of a car.
#If he exceeds 80 km / h show a message saying he was fined.
#The fine will cost R $ 7.00 for each km above the limit

#Escreva um programa que leia a velocidade de um carro.
#Se ele ultrapassar 80Km/h mostre uma mensagem dizendo que ele foi multado.
#A multa vai custar R$7,00 por cada km acima do limite


velocity = float(input('How fast was the car? '))
traffic_ticket = (velocity - 80) * 7
if  velocity > 80:
    print('Você foi multado!')
    print(f'The value of your fine is R${traffic_ticket},00')
else:
    print(f'You passed there {velocity} KMs, which is an allowed speed on the road!')


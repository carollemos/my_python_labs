#Create a program that reads the name of a city and says whether or not it begins with the name "SANTO".

#Crie um programa que leia o nome de uma cidade e diga se ela começa com ou não o nome "SANTO".

city = input('What is city name? ').strip().lower()
holy = city[:5]
if city == holy:
    print('Your city begins with SANTO.')
else:
    print('Your city dont begins with SANTO.')
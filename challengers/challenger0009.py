# Make a program that reads any integer and shows your multiplication table.

#Faça um programa que leia um número inteiro qualquer e mostre sua tabuada.

number = int(input("Write an integer: "))

print('=' * 12)
print('{} x {} = {}'.format(number, 1, number*1))
print('{} x {} = {}'.format(number, 2, number*2))
print('{} x {} = {}'.format(number, 3, number*3))
print('{} x {} = {}'.format(number, 4, number*4))
print('{} x {} = {}'.format(number, 5, number*5))
print('{} x {} = {}'.format(number, 6, number*6))
print('{} x {} = {}'.format(number, 7, number*7))
print('{} x {} = {}'.format(number, 8, number*8))
print('{} x {} = {}'.format(number, 9, number*9))
print('{} x {} = {}'.format(number, 10, number*10))
print('=' * 12)

print('>>>Complete multiplication table!<<<')
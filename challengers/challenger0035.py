#Develop a program that reads the length of three lines and tells the user if they
# may or may not form a triangle.

#Desenvolva um programa que leia o comprimento de três retas e diga o usuário se elas
#podem ou não formar um triângulo.

line1 = float(input('Enter the first line: '))
line2 = float(input('Enter the second line: '))
line3 = float(input('Enter the third line: '))
if line1 < line2 + line3 and line2 < line1 + line3 and line3 < line1 + line2:
    print('The above segments can form a triangle!')
else:
    print('The above segments cannot form a triangle!')
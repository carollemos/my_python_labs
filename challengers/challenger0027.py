# Make a program that reads a person's full name, then shows
#the first and last name separately.
#EX: Ana Maria de Souza
# First: Ana
# Last: Souza

# Faça um programa que leia o nome completode uma pessoa, mostrando em seguida
#o primeiro e o último nome separadamente.
#EX: Ana Maria de Souza
# Primeiro: Ana
# Último: Souza

name = input("Write your full name: ").strip().split()

print(f'Your first name is: {name[0]}')

print(f'Your last name is: {name[len(name)-1]}')








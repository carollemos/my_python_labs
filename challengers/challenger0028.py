# Write a program that makes a computer "think" of an integer
# between 0 and 5 and ask the user to try to find out which number was chosen
# by the computer.

# The program should write on the screen if the user won or asked.

#Escreva um programa que faça um computador "pensar" em um número inteiro
# entre 0 e 5 e peça para o usuário tentar descobrir qual foi o número escolhido
#pelo computador.

#O programa deverá escrever na tela se o usuário venceu ou pedeu.

from random import choice
from time import sleep
number = input('Enter a number and see if it was the same as the computer thought: ')
list = ['1', '2', '3', '4', '5']
selected = choice(list)

if selected == number:
    print('Processing....')
    sleep(3)
    print('You are right!')
else:
    print('Processando....')
    sleep(3)
    print('This was not the number I thought, try again!')
    print(f'The number I thought was: {selected}')

print('=== Bye, Bye! ====')


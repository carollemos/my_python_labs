#Create a program that reads how much money a person has in their wallet and shows how many dollars they can buy.

#Crie um programa que leia quanto de dinheiro uma pessoa tem na carteira e mostre quantos dólares ela pode comprar.

reais = float(input('Helo! How much money do you have in your wallet?'))
dollar = reais / 5.78 #Value of the dollar quotation on the day 29.03.2021

print('You have R${:.2f} or ${:.2f}'.format(reais, dollar))

print('Bye, Bye!')
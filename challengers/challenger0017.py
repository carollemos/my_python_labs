# Make a program that reads the length of the opposite leg and the adjacent leg of a triangle
# rectangle. Calculate and show the length of the hypotenuse.

# Faça um programa que leia o comprimento do cateto oposto e do cateto adjacente de um triângulo
# retângulo. Calcule e mostre o comprimento da hipotenusa.

# Teorema de Pitágoras: a²=b²+c²

import math

opposite_side = float(input('Enter the opposite side: '))
adjacent_side = float(input('Enter the adjacent side: '))
hypotenuse = math.hypot(adjacent_side, opposite_side)

print('=== The sum of the side squares is equal to the square of the hypotenuse ===')
print('=== A soma dos quadrados dos catetos é igual ao quadrado da hipotenusa ===')

print(f'The hypotenuse length value is: {hypotenuse:.2f}')

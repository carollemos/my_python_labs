# Make a program that reads the width and height of a wall in meters, calculate its area and its amount of paint
# necessary to paint it, knowing that each liter of paint paints an area of 2m².

#Faça um programa que leia a largura e a altura de uma parede em metros, calcule a sua área e a sua quantidade de tinta
#necessária para pintá-la, sabendo que cada litro de tinta pinta uma área de 2m².

wall_height = float(input('Enter wall height: '))
wall_width = float(input('Enter the width of the wall: '))
area = wall_height * wall_width
ink = area / 2

print('Your area is: \n {:.2f}m² and you need to {:.2f} liters of paint.'.format(area, ink))